import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class GetDataService {
  url = "http://localhost:8080/getalldata"
  constructor(private _http: HttpClient) { }

  getData()
  {
    return this._http.get(this.url);
  }

}
