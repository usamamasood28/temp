import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  public obj;

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.obj = JSON.parse( this.route.snapshot.paramMap.get('obj'))
    //console.log(this.obj)
  }

  edit(){
    console.log(this.obj)
    this.router.navigate(['/form',JSON.stringify(this.obj)])
  }

}
