import { TestBed } from '@angular/core/testing';

import { DeleteIdDataService } from './delete-id-data.service';

describe('DeleteIdDataService', () => {
  let service: DeleteIdDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DeleteIdDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
