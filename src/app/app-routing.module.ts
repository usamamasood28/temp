import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormComponent } from './form/form.component';
import { TableViewComponent } from './table-view/table-view.component';
import { CardViewComponent } from './card-view/card-view.component';
import { ProfileComponent } from './profile/profile.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  { path: '', component: FormComponent },
  { path: 'form', component: FormComponent },
  { path: 'form/:obj', component: FormComponent },
  { path: 'profile/:obj', component: ProfileComponent },
  { path: 'tablecrudview', component: TableViewComponent },
  { path: 'cardslayoutview', component: CardViewComponent },
  { path: 'login', component: LoginComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [ TableViewComponent, CardViewComponent, FormComponent, ProfileComponent, LoginComponent ]