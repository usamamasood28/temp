import { Component, OnInit } from '@angular/core';
import  { GetDataService }  from '../get-data.service'
import  {DeleteIdDataService}  from '../delete-id-data.service'
import { Router, RouterLink } from '@angular/router';

@Component({
  selector: 'app-card-view',
  templateUrl: './card-view.component.html',
  styleUrls: ['./card-view.component.css']
})
export class CardViewComponent implements OnInit {

  public arr

  constructor(private _getDataService: GetDataService, private _deleteById : DeleteIdDataService, private router: Router) { }

  ngOnInit(): void {
    this._getDataService.getData()
    .subscribe((data)=>{
      this.arr=data[0];
    })
  }

  profileFunction(id, event)
  {
    //console.log(id)
    var obj = this.arr.filter(x=>x.Id==id)
    //console.log(obj)
    //this._deleteById.deleteById(id);
    this.router.navigate(['/profile',JSON.stringify(obj[0])])
  }

}
