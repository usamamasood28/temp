import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class DeleteIdDataService {

  url = "http://localhost:8080/deleteiddata/"
  public obj
  constructor(private _http: HttpClient) { }

  deleteById(id)
  {
    this.obj={ "id": id.toString() }
    //console.log(id.toString())
    //var obj = { id: id.toString() }
    return this._http.delete(this.url+id.toString())
    .subscribe({
      error: error=>console.log(error)
    })
  }

}
