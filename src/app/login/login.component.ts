import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import  { GetDataService }  from '../get-data.service'
import { Router, RouterLink } from '@angular/router';
import { AppComponent } from '../app.component'
import { GlobalVarsService } from "../global-vars.service"

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public arr
  constructor(private fb: FormBuilder, private _getDataService: GetDataService, private router: Router, private gv: GlobalVarsService) { }

  adminForm = this.fb.group({
    aemail: ['', Validators.required],
    apass: ['', Validators.required]
  })

  userForm = this.fb.group({
    uemail: ['', Validators.required],
    upass: ['', Validators.required]
  })

  ngOnInit(): void {
    this._getDataService.getData()
    .subscribe((data)=>{
      this.arr=data[0];
    })
  }

  onuserSubmit()
  {
    var found=0;
    for(var item of this.arr)
    {
      if(item.email==this.userForm.value.uemail && item.pass==this.userForm.value.upass)
      {
        //alert("logging in")
        this.gv.isUserLoggedIn.next(true);
        this.router.navigate(['/profile',JSON.stringify(item)])
        found=1;
      }
      break;
    }
    if(!found)
    {
      alert("Wrong Username or Password")
    }
  }

  onadminSubmit()
  {
    if(this.adminForm.value.aemail=="admin" && this.adminForm.value.apass=="admin")
    {
      this.router.navigateByUrl("/tablecrudview")
      this.gv.isUserLoggedIn.next(true);
    }
  }

}
