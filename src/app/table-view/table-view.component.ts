import { Component, OnInit } from '@angular/core';
import  { GetDataService }  from '../get-data.service'
import  {DeleteIdDataService}  from '../delete-id-data.service'
import { Router, RouterLink } from '@angular/router';


@Component({
  selector: 'app-table-view',
  templateUrl: './table-view.component.html',
  styleUrls: ['./table-view.component.css']
})
export class TableViewComponent implements OnInit {
  public arr
  constructor(private _getDataService: GetDataService, private _deleteById : DeleteIdDataService, private router: Router) {
  }

  ngOnInit(): void {
    this._getDataService.getData()
    .subscribe((data)=>{
      this.arr=data[0];
    })
    //console.log(this.arr)
  }
  
  deleteFunction(id, event)
  {
    //console.log(id)
    this._deleteById.deleteById(id);
    //window.location.reload();
    var td = event.target.parentNode;
    var tr = td.parentNode;
    tr.parentNode.removeChild(tr);
  }

  editFunction(id, event)
  {
    //console.log(id)
    var obj = this.arr.filter(x=>x.Id==id)
    //console.log(obj)
    //this._deleteById.deleteById(id);
    this.router.navigate(['/form',JSON.stringify(obj[0])])
  }

  profileFunction(id, event)
  {
    //console.log(id)
    var obj = this.arr.filter(x=>x.Id==id)
    //console.log(obj)
    //this._deleteById.deleteById(id);
    this.router.navigate(['/profile',JSON.stringify(obj[0])])
  }
  cardview()
  {
    this.router.navigateByUrl("/cardslayoutview")
  }

}
