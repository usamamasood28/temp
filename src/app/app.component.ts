import { Component } from '@angular/core';
import { GlobalVarsService } from './global-vars.service'
import { Router, RouterLink } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  isUserLoggedIn: boolean;
  constructor(private gv: GlobalVarsService, private router: Router){
    this.gv.isUserLoggedIn.subscribe(value=>{
      this.isUserLoggedIn = value;
    })
  }
  logout()
  {
    this.gv.isUserLoggedIn.next(false);
    this.router.navigateByUrl("/login")
  }
}
