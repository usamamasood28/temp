export interface StudentData {
    fname: string,
    lname: string,
    email: string,
    address: string,
    gender: string,
    city: string,
    phno: string,


    miname: string,
    mmajors: string,
    mtmarks: string,
    momarks: string,
    mper: string,

    ciname: string,
    cmajors: string,
    ctmarks: string,
    comarks: string,
    cper: string,

    
    uiname: string,
    udname: string,
    cgpa: string

}
