import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SaveDataService {

  _url = "http://localhost:8080/savedata/";
  constructor(private _http: HttpClient) { }

  saveData(userData)
  {
    if(userData.Id)
    {
      return this._http.put<any>(this._url+userData.Id, userData);
    }
    else
    {
      return this._http.post<any>(this._url, userData);
    }
  }

}
