import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router'
import { SaveDataService } from '../save-data.service'


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  public obj;

  constructor(private fb: FormBuilder, private _saveDataService: SaveDataService, private route: ActivatedRoute) { }

  studentMgmtForm = this.fb.group({

    fname: ['', Validators.required],
    lname: ['', Validators.required],
    email: ['', [Validators.email, Validators.required]],
    pass: ['', [Validators.required, Validators.minLength(5)]],
    address: ['', Validators.required],
    gender: ['', Validators.required],
    city: ['', Validators.required],
    phno: ['', [Validators.required, Validators.minLength(11), Validators.maxLength(11)]],


    miname: ['', Validators.required],
    mmajors: ['', Validators.required],
    mtmarks: ['', Validators.required],
    momarks: ['', Validators.required],
    mper: ['', [Validators.required, Validators.min(0), Validators.max(100)]],

    ciname: ['', Validators.required],
    cmajors: ['', Validators.required],
    ctmarks: ['', Validators.required],
    comarks: ['', Validators.required],
    cper: ['', [Validators.required, Validators.min(0), Validators.max(100)]],

    
    uiname: ['', Validators.required],
    udname: ['', Validators.required],
    cgpa: ['', [Validators.required, Validators.min(1), Validators.max(4)]]
  });

  ngOnInit(): void {
    this.obj = JSON.parse( this.route.snapshot.paramMap.get('obj'))
    if(this.obj)
    {
      this.editFill(this.obj);
    }
  }

  editFill(obj)
  {
    console.log(obj)
    this.studentMgmtForm.patchValue({
      fname: obj.fname,
      lname: obj.lname,
      pass: obj.pass,
      email: obj.email,
      address: obj.address,
      gender: obj.gender,
      city: obj.city,
      phno: obj.phno,


      miname: obj.miname,
      mmajors: obj.mmajors,
      mtmarks: obj.mtmarks,
      momarks: obj.momarks,
      mper: obj.mper,

      ciname: obj.ciname,
      cmajors: obj.cmajors,
      ctmarks: obj.ctmarks,
      comarks: obj.comarks,
      cper: obj.cper,

    
      uiname: obj.uiname,
      udname: obj.udname,
      cgpa: obj.cgpa

    });
  }

  onSubmit(){
    if(this.studentMgmtForm.invalid)
    {
      if(this.studentMgmtForm.get('email').invalid)
      {
        alert("Please fill out proper email address")
      }
      else{
        alert("Please fill out all credentials")
      }
    }
    else{
      alert("User Data Saved")
      //console.log(this.studentMgmtForm.value)
      var st = this.studentMgmtForm.value;
      if(this.obj)
      {
        st.Id = this.obj.Id
      }
      console.log(st)
      this._saveDataService.saveData(st)
      .subscribe(
        response => console.log("success", response),
        error => console.log("error",error)
      )
    }
    this.studentMgmtForm.reset();
  }

}
